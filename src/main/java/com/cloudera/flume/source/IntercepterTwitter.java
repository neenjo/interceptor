package com.cloudera.flume.source;

import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.interceptor.Interceptor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nithin on 15/5/15.
 */
public class IntercepterTwitter implements Interceptor {

    @Override
    public void initialize() {

    }

    @Override
    public Event intercept(Event event) {
        System.out.println(event.getBody());
        return null;
    }

    @Override
    public List<Event> intercept(List<Event> events) {
        List<Event> interceptedEvents =
                new ArrayList<Event>(events.size());
        for (Event event : events) {
            Event interceptedEvent = intercept(event);
            interceptedEvents.add(interceptedEvent);
        }
        return null;
    }

    @Override
    public void close() {

    }

    public static class Builder
            implements Interceptor.Builder {

        @Override
        public Interceptor build() {
            return new IntercepterTwitter();
        }

        @Override
        public void configure(Context context) {

        }
    }
}
